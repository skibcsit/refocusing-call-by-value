trait Term   {
  def equal(b:Term):Boolean= {
    this match {
      case a: Variable => {
        b match {
          case b: Variable => if ((a!=null)&&(b!=null)) return (a.value == b.value) else return (a==b)
          case _=>return false
        }
      }
      case a: Application => {
        b match {
          case b: Application => if  ((a!=null)&&(b!=null)) return ((a.app1.equal(b.app1)) && (a.app2.equal(b.app2))) else return (a==b)
          case _=>return false
        }
      }
      case a: Abstraction => {
        b match {
          case b: Abstraction => if  ((a!=null)&&(b!=null)) return ((a.value == b.value) && (a.expression.equal(b.expression))) else return (a==b)
          case _=>return false
        }
      }
    }
  }
}
