

object Main {
  def parse(str: String): Term = {
    if (str.length().equals(0)) {
      return null;
    }
    if (str.length.equals(1)) {
      val v: Variable = new Variable(str.charAt(0));
      return v;
    }
    if (str.charAt(0).equals('\\')) {
      val v1: Term = parse(str.substring(3));
      val v: Abstraction = new Abstraction(str.charAt(1), v1);
      return v;
    }
    if ((str.charAt(str.length() - 1).equals(')'))) {
      var a: Int = 0;
      var i: Int = str.length() - 2;
      while (!((a.equals(0)) && (str.charAt(i).equals('(')))) {
        if (str.charAt(i).equals(')')) a = a + 1;
        if (str.charAt(i).equals('(')) a = a - 1;
        i = i - 1;
      }
      var v1: Term = null
      if (i != 0) v1 = parse(str.substring(0, i)) else v1 = null;
      var v2: Term = null;
      if (i + 1 <= str.length() - 1) v2 = parse(str.substring(i + 1, str.length() - 1)) else v2 = null;
      if (v1 == null) return v2
      else if (v2 == null) return v1
      else {
        val v: Application = new Application(v1, v2);
        return v;
      }
    }
    val v1: Variable = new Variable(str.charAt(str.length() - 1));
    val v2: Term = parse(str.substring(0, str.length() - 1));
    if (v1 == null) return v2
    else if (v2 == null) return v1
    else {
      val v: Application = new Application(v2, v1);
      return v;
    }
  }


  def termOut(t: Term): Unit = {
    t match {
      case a: Application => {
        printf("App(");
        termOut(a.app1);
        printf(",");
        termOut(a.app2);
        printf(")")
      }
      case a: Variable => {
        printf("%c", a.value);
      }
      case a: Abstraction => {
        printf("\\%c.", a.value);
        termOut(a.expression);
      }
    }

  }

  def main(args: Array[String]): Unit = {
    var str: String = io.StdIn.readLine();
    var term: Term = parse(str);
    termOut(term);
    printf("\n")
    var a: Long = System.currentTimeMillis()
    termOut(REStas.evaluate(term))
    a = System.currentTimeMillis() - a
    printf("\n%d\n", a)
    a = System.currentTimeMillis()
    termOut(DRPCircle.evaluate(term))
    a = System.currentTimeMillis() - a
    printf("\n%d", a)
  }
}
