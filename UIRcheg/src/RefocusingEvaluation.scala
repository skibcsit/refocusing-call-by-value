object RefocusingEvaluation {
  def evaluate(t:Term):Term=refocus(t,null)
  def reduction(x:Term,c:Char,y:Term):Term={
    y match{
      case a:Variable=>if (a.value==c) return x else return a;
      case a:Application=>return new Application(reduction(x,c,a.app1),reduction(x,c,a.app2))
      case a:Abstraction=>if (a.value!=c) return new Abstraction(a.value,reduction(x,c,a.expression)) else return a;
    }
  }
  def refocus(t:Term,c:Context):Term={
    t match{
      case a:Value => {
        if (c ==null) return a
        if  ((c.app1==null) && (c.app2!=null)) {
          c.app1=a;
          var x:Term=c.app2;
          c.app2=null;
          return refocus(x,c);
        }
        c.app1 match{
          case b:Abstraction => return reduction (a,b.value,b.expression);
          case b:Variable =>{
            val x:Application=new Application(b,a)
            return x;
          }
        }
      }

      case a:Application =>{
        val x:Context=new Context(null,a.app2)
        var term:Term=refocus(a.app1,x);
        if (term.equal(a)) {
          if (c==null) return term
          if (c.app1==null) return new Application(term,c.app2)
          if (c.app2==null){
              c.app1 match{
                case b:Abstraction=>term=reduction(a,b.value,b.expression)
                case _=>return new Application(c.app1,term)
              }

          }
        }
        return refocus(term,c)
      }
    }
  }
}
