

class Pluger(t:Term){

  private var term:Term=t

  def getTerm():Term=term

  def plug(c:Context):Unit={
    if (c==null) return
    if (c.app1==null) term=new Application(term,c.app2)
    if (c.app2==null) term=new Application(c.app1,term)
  }

}



object DRPCircle {

  def evaluate(t:Term):Term={
    var term:Term=t
    term=decompose(term,null).getTerm()
    if (term.equal(t)) return term else return evaluate(term);
  }

  def reduction(x: Term, c: Char, y: Term): Term = {
    y match {
      case a: Variable => if (a.value == c) return x else return a;
      case a: Application => return new Application(reduction(x, c, a.app1), reduction(x, c, a.app2))
      case a: Abstraction => if (a.value != c) return new Abstraction(a.value, reduction(x, c, a.expression)) else return a;
    }
  }

  def decompose(t:Term, c:Context):Pluger={
    t match{
      case a:Value => {
        if (c ==null) return new Pluger(a)
        if  ((c.app1==null) && (c.app2!=0)) {
          c.app1=a;
          var x:Term=c.app2;
          c.app2=null;
          return decompose(x,c);
        }
        c.app1 match{
          case b:Abstraction => return new Pluger(reduction (a,b.value,b.expression));
          case b:Variable =>{
            val x:Application=new Application(b,a)
            return new Pluger(x);
          }
        }
      }

      case a:Application => {
        if (c != null) c.app1 match {
          case b: Abstraction => return new Pluger(reduction(a, b.value, b.expression))
          case _ =>
        }
        var x: Pluger = decompose(a.app1, new Context(null, a.app2))
        x.plug(c)
        return x
      }
    }
  }
}
